package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[][] matriz = new int[3][3];
        for (int x=0; x < matriz.length; x++) {
            for (int y=0; y < matriz[x].length; y++) {
                matriz[x][y] = (int) (Math.random()*100) + 1;
            }
            }
        System.out.println(Arrays.deepToString(matriz).replace("], ", "]\n").replace("[[", "[").replace("]]", "]"));
        }

}
