package com.company;

public class RaizScanner {
    int numero;
    public RaizScanner (int numero){
        this.numero = numero;
    }
    public int getNumero(){
        return numero;
    }
    public void setNumero(int numero){
        this.numero = numero;
    }
    public double getRaiz (){
        double raizcuadrada = Math.sqrt(this.numero);
        return raizcuadrada;
    }

}
