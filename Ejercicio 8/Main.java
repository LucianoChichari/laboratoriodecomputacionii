package com.company;
import java.util.Scanner;
import java.lang.Math;

public class Main {

    public static void main(String[] args) {
        Scanner scan= new Scanner(System.in);
        System.out.println("Introduce un numero: ");
        RaizScanner num = new RaizScanner(scan.nextInt());
        System.out.println("La raiz de " + num.getNumero() + " es = " + num.getRaiz());
    }
}
