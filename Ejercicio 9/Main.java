package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int altura, pesoideal;
        String sexo;

        Scanner scan= new Scanner(System.in);
        System.out.println("Ingrese Altura en centimetros: ");
        altura = scan.nextInt();
        System.out.println("Ingrese Sexo H/Hombre M/Mujer: ");
        sexo = scan.next().toUpperCase();
        scan.close();

        if (sexo.equals("M")){
            pesoideal= altura-120;
            System.out.println("Su peso ideal es: "+ pesoideal);

        }else if (sexo.equals("H")){
            pesoideal= altura-110;
            System.out.println("Su peso ideal es: "+ pesoideal);
        }

        else{
            System.out.println("Sexo invalido");
        }

    }
}
